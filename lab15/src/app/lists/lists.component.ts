import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Person } from '../shared/person.model';
import { MainService } from '../shared/services/main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})

export class ListsComponent implements OnInit {
  searchString='';
  persons: Person[] = [];
  constructor(private mainService: MainService, private router: Router){
  }
  
  ngOnInit() {  
    this.doGet('/Persons');     
  }
  async doGet(url){
    try {
      let res = await this.mainService.getData(url);
      if (typeof res !== 'undefined') {
        for (const man in res) {
          this.persons.push(new Person(res[man].id, res[man].name, res[man].surname, res[man].phone));
        }
      }
    } catch (err) {
      console.log(err);
    }
  }
  onLink(id){
    this.router.navigate(['/item', id]);
  }
}