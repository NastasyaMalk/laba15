import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Person } from '../shared/person.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MainService } from '../shared/services/main.service';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-view-person',
  templateUrl: './view-person.component.html',
  styleUrls: ['./view-person.component.css']
})
export class ViewPersonComponent implements OnInit {
  @Output() changeP = new EventEmitter<Person>();
  @Output() deleteP = new EventEmitter<number>();
  editForm: FormGroup;
  id: number;
  selectedItem: any;
  person;
  
  constructor(private activateRouter: ActivatedRoute,
    private mainService: MainService, private router: Router) {
   this.activateRouter.params.subscribe(param => {
     this.id = +param.id;
   });
  }
  flag=true;
  async onEdit(id){
    if (!this.flag){
      let newPerson = new Person(id, this.editForm.value.name, this.editForm.value.surname, this.editForm.value.phone);
      this.doPut('/Persons', id, newPerson);
      this.person = await this.mainService.getData(`/Persons/${this.id}`);
    }
    this.flag=!this.flag;
  }
  exist=true;
  async ngOnInit() {
      try {
        this.person = await this.mainService.getData(`/Persons/${this.id}`);
        if (this.person.isNullOrUndefined) {this.exist=false}
      } catch (err) {
        console.log(err);
      }
      if (this.exist ){
        console.log(this.person);
        this.editForm = new FormGroup({
            name: new FormControl(this.person.name, [Validators.required, Validators.pattern(/[A-Za-zА-Яа-яёЁ]/)]),
            surname: new FormControl(this.person.surname, [Validators.required, Validators.pattern(/[A-Za-zА-Яа-яёЁ]/)]),
            phone: new FormControl(this.person.phone, [Validators.required, Validators.pattern(/[0-9]/), Validators.minLength(10), Validators.maxLength(11)])
          });
      }
  }
  async doPut(url, id, obj){
    try {
      await this.mainService.putData(url, id, obj);
      
    } catch (err) {
      console.log(err);
    }
  }
  async doDelete(url, id){
    try {
      await this.mainService.deleteData(url, id);
      this.router.navigate(['/']);
    } catch (err) {
      console.log(err);
    }
  }
}